//
// function.groovy
//

def int    add(a, b)     { a + b }
//def String combine(a, b) { a + " " + b }
def String combine(a, b) { "${a} ${b}" }

def x = add(40, 2)
def y = combine("is", combine("the","answer"))

println combine(x, y)
